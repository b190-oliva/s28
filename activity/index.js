//insert
db.rooms.insertOne({
    name: "single",
    accomodates: 2,
    price: 1000,
    description: "A simple room will all the basic necessities",
    roomsAvailable: 10,
    isAvailable: false
});

// insert many
db.rooms.insertMany([
    {
        name: "double",
        accomodates: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation",
        roomsAvailable: 5,
        isAvailable: false
    },
    {
        name: "queen",
        accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for simple getaway",
        roomsAvailable: 15,
        isAvailable: false
    }
]);

//find

db.rooms.find({
	name: "double"
});

//updateone

db.rooms.updateOne(
    { name: "queen" },
    {
        $set : {
            roomsAvailable: 0
        }
    }
);

//delete rooms with 0 availability

db.rooms.deleteMany({ 
    roomsAvailable: 0
});


